locals {
  tags = {
    Application = "plum tree"
  }
}

provider "aws" {
  region = "eu-west-1"
}

terraform {
  backend "s3" {
    bucket = "plum-tree-state"
    key    = "blue-green/terraform.tfstate"
    region = "eu-west-1"
  }
}

module "blue_green" {
  source = "../modules/blue-green"
  color  = var.color
  tags   = local.tags
}
