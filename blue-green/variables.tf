variable "application_name" {
  type        = string
  description = "The application name"
  default     = "core"
}

variable "color" {
  type        = string
  description = "The color to release"
}