resource "aws_dynamodb_table" "donations_cache" {
  name           = "DonationsCache"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "YearMonth"

  attribute {
    name = "YearMonth"
    type = "S"
  }

  tags = {
    Name = "DonationsCache"
  }
}
