output "aws_account_id" {
  value = "741172697449"
}

output "vpc_id" {
  value = module.network.vpc_id
}

output "region" {
  value = "eu-west-1"
}

output "vpc_cidr" {
  value = "172.17.0.0/20"
}

output "public_subnets" {
  value = module.network.public_subnets
}

output "private_subnets" {
  value = module.network.private_subnets
}
