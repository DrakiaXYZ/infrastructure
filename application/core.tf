locals {
  tags = {
    Application = "plum tree",
    Environment = terraform.workspace
  }
  name_prefix = "plum-tree-${terraform.workspace}"
}

provider "aws" {
  region = "eu-west-1"
}

terraform {
  backend "s3" {
    bucket = "plum-tree-state"
    key    = "application/terraform.tfstate"
    region = "eu-west-1"
  }
}

data "terraform_remote_state" "network" {
  backend   = "s3"

  config = {
    bucket = "plum-tree-state"
    key    = "network/terraform.tfstate"
    region = "eu-west-1"
  }
}
