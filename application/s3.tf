# Statics bucket for frontend
resource "aws_s3_bucket" "static_assets" {
  bucket = "com.theplumtreeapp.${terraform.workspace}-assets"
  tags   = local.tags
}

resource "aws_s3_bucket_public_access_block" "static_assets" {
  block_public_acls       = true
  block_public_policy     = true
  bucket                  = aws_s3_bucket.static_assets.id
  ignore_public_acls      = true
  restrict_public_buckets = true
}
