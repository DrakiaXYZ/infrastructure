resource "aws_api_gateway_rest_api" "api" {
  description              = "API Gateway for the Plum Tree ${terraform.workspace}"
  name                     = local.name_prefix
  minimum_compression_size = 0
  binary_media_types       = ["*/*"]

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

# ########### #
# Healthcheck #
# ########### #

resource "aws_api_gateway_resource" "healthcheck" {
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "healthcheck"
  rest_api_id = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_method" "healthcheck" {
  authorization = "NONE"
  http_method   = "PATCH"
  resource_id   = aws_api_gateway_resource.healthcheck.id
  rest_api_id   = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_integration" "healthcheck" {
  http_method = aws_api_gateway_method.healthcheck.http_method
  resource_id = aws_api_gateway_resource.healthcheck.id
  rest_api_id = aws_api_gateway_rest_api.api.id
  type        = "MOCK"

  request_templates = {
    "application/json" = jsonencode({
      statusCode = 200
    })
  }
}

resource "aws_api_gateway_method_response" "healthcheck" {
  http_method = aws_api_gateway_method.healthcheck.http_method
  resource_id = aws_api_gateway_resource.healthcheck.id
  rest_api_id = aws_api_gateway_rest_api.api.id
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "healthcheck" {
  http_method = aws_api_gateway_method.healthcheck.http_method
  resource_id = aws_api_gateway_resource.healthcheck.id
  rest_api_id = aws_api_gateway_rest_api.api.id
  status_code = aws_api_gateway_method_response.healthcheck.status_code

  response_templates = {
    "application/json" = jsonencode({
      statusCode = 200
      message    = "Healthy"
    })
  }
}

# ################### #
# Uploads (Processed) #
# ################### #

data "aws_s3_bucket" "uploads_bucket" {
  bucket = "com.theplumtreeapp.upload-processed"
}

data "aws_iam_policy_document" "uploads_s3_policy" {
  statement {
    actions = [
      "s3:Get*",
      "s3:List*",
    ]

    resources = [
      data.aws_s3_bucket.uploads_bucket.arn,
      "${data.aws_s3_bucket.uploads_bucket.arn}/*"
    ]
  }
}

resource "aws_iam_policy" "uploads_s3_policy" {
  name        = "${local.name_prefix}-uploads-bucket-access"
  description = "Read only access for Plum Tree ${terraform.workspace} uploads bucket"
  policy      = data.aws_iam_policy_document.uploads_s3_policy.json
}

data "aws_iam_policy_document" "uploads_s3_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["apigateway.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "uploads_s3_api_gateyway_role" {
  name = "${local.name_prefix}-gateway-uploads-s3-role"
  assume_role_policy = data.aws_iam_policy_document.uploads_s3_assume_role.json
}

resource "aws_iam_role_policy_attachment" "uploads_s3_policy_attach" {
  role       = aws_iam_role.uploads_s3_api_gateyway_role.name
  policy_arn = aws_iam_policy.uploads_s3_policy.arn
}

resource "aws_api_gateway_resource" "uploads" {
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "uploads"
  rest_api_id = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_resource" "uploads_item" {
  parent_id   = aws_api_gateway_resource.uploads.id
  path_part   = "{item+}"
  rest_api_id = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_method" "uploads_item" {
  authorization = "NONE"
  http_method   = "GET"
  resource_id   = aws_api_gateway_resource.uploads_item.id
  rest_api_id   = aws_api_gateway_rest_api.api.id

  request_parameters = {
    "method.request.header.If-Modified-Since" = false,
    "method.request.header.If-None-Match"     = false,
    "method.request.path.item"                = true
  }
}

resource "aws_api_gateway_integration" "uploads_item" {
  http_method             = aws_api_gateway_method.uploads_item.http_method
  resource_id             = aws_api_gateway_resource.uploads_item.id
  rest_api_id             = aws_api_gateway_rest_api.api.id
  type                    = "AWS"
  integration_http_method = "GET"
  uri                     = "arn:aws:apigateway:eu-west-1:s3:path/${data.aws_s3_bucket.uploads_bucket.id}/{item}"
  credentials             = aws_iam_role.uploads_s3_api_gateyway_role.arn

  request_parameters = {
    "integration.request.header.If-Modified-Since" = "method.request.header.If-Modified-Since"
    "integration.request.header.If-None-Match"     = "method.request.header.If-None-Match"
    "integration.request.path.item"                = "method.request.path.item"
  }
}

resource "aws_api_gateway_method_response" "uploads_item" {
  http_method = aws_api_gateway_method.uploads_item.http_method
  resource_id = aws_api_gateway_resource.uploads_item.id
  rest_api_id = aws_api_gateway_rest_api.api.id
  status_code = "200"

  response_parameters = {
    "method.response.header.Cache-Control"             = true,
    "method.response.header.Content-Length"            = true,
    "method.response.header.Content-Type"              = true,
    "method.response.header.ETag"                      = true,
    "method.response.header.Last-Modified"             = true,
  }
}

resource "aws_api_gateway_integration_response" "uploads_item" {
  http_method      = aws_api_gateway_method.uploads_item.http_method
  resource_id      = aws_api_gateway_resource.uploads_item.id
  rest_api_id      = aws_api_gateway_rest_api.api.id
  status_code      = aws_api_gateway_method_response.uploads_item.status_code
  content_handling = "CONVERT_TO_BINARY"

  response_parameters = {
    "method.response.header.Cache-Control"             = "'max-age=31536000'"
    "method.response.header.Content-Length"            = "integration.response.header.Content-Length",
    "method.response.header.Content-Type"              = "integration.response.header.Content-Type",
    "method.response.header.ETag"                      = "integration.response.header.ETag",
    "method.response.header.Last-Modified"             = "integration.response.header.Last-Modified",
  }

  depends_on = [aws_api_gateway_integration.uploads_item]
}

resource "aws_api_gateway_method_response" "cached_uploads_item" {
  http_method = aws_api_gateway_method.uploads_item.http_method
  resource_id = aws_api_gateway_resource.uploads_item.id
  rest_api_id = aws_api_gateway_rest_api.api.id
  status_code = "304"

  response_parameters = {
    "method.response.header.Cache-Control"  = true,
    "method.response.header.ETag"           = true,
    "method.response.header.Last-Modified"  = true,
  }
}

resource "aws_api_gateway_integration_response" "cached_uploads_item" {
  http_method       = aws_api_gateway_method.uploads_item.http_method
  resource_id       = aws_api_gateway_resource.uploads_item.id
  rest_api_id       = aws_api_gateway_rest_api.api.id
  status_code       = aws_api_gateway_method_response.cached_uploads_item.status_code
  content_handling  = "CONVERT_TO_BINARY"
  selection_pattern = "304"

  response_parameters = {
    "method.response.header.Cache-Control"  = "'max-age=31536000'",
    "method.response.header.ETag"           = "integration.response.header.ETag",
    "method.response.header.Last-Modified"  = "integration.response.header.Last-Modified",
  }

  depends_on = [aws_api_gateway_integration.uploads_item]
}

# ################### #
# Uploads (Origonals) #
# ################### #

data "aws_s3_bucket" "uploads_input_bucket" {
  bucket = "com.theplumtreeapp.upload-input"
}

data "aws_iam_policy_document" "uploads_input_s3_policy" {
  statement {
    actions = [
      "s3:Get*",
      "s3:List*",
    ]

    resources = [
      data.aws_s3_bucket.uploads_input_bucket.arn,
      "${data.aws_s3_bucket.uploads_input_bucket.arn}/*"
    ]
  }
}

resource "aws_iam_policy" "uploads_input_s3_policy" {
  name        = "${local.name_prefix}-uploads-input-bucket-access"
  description = "Read only access for Plum Tree ${terraform.workspace} uploads input bucket"
  policy      = data.aws_iam_policy_document.uploads_input_s3_policy.json
}

data "aws_iam_policy_document" "uploads_input_s3_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["apigateway.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "uploads_input_s3_api_gateyway_role" {
  name = "${local.name_prefix}-gateway-uploads-input-s3-role"
  assume_role_policy = data.aws_iam_policy_document.uploads_input_s3_assume_role.json
}

resource "aws_iam_role_policy_attachment" "uploads_input_s3_policy_attach" {
  role       = aws_iam_role.uploads_input_s3_api_gateyway_role.name
  policy_arn = aws_iam_policy.uploads_input_s3_policy.arn
}

resource "aws_api_gateway_resource" "uploads_original" {
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "uploads-orig"
  rest_api_id = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_resource" "uploads_original_item" {
  parent_id   = aws_api_gateway_resource.uploads_original.id
  path_part   = "{item+}"
  rest_api_id = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_method" "uploads_original_item" {
  authorization = "NONE"
  http_method   = "GET"
  resource_id   = aws_api_gateway_resource.uploads_original_item.id
  rest_api_id   = aws_api_gateway_rest_api.api.id

  request_parameters = {
    "method.request.header.If-Modified-Since" = false,
    "method.request.header.If-None-Match"     = false,
    "method.request.path.item"                = true
  }
}

resource "aws_api_gateway_integration" "uploads_original_item" {
  http_method             = aws_api_gateway_method.uploads_original_item.http_method
  resource_id             = aws_api_gateway_resource.uploads_original_item.id
  rest_api_id             = aws_api_gateway_rest_api.api.id
  type                    = "AWS"
  integration_http_method = "GET"
  uri                     = "arn:aws:apigateway:eu-west-1:s3:path/${data.aws_s3_bucket.uploads_input_bucket.id}/{item}"
  credentials             = aws_iam_role.uploads_input_s3_api_gateyway_role.arn

  request_parameters = {
    "integration.request.header.If-Modified-Since" = "method.request.header.If-Modified-Since"
    "integration.request.header.If-None-Match"     = "method.request.header.If-None-Match"
    "integration.request.path.item"                = "method.request.path.item"
  }
}

resource "aws_api_gateway_method_response" "uploads_original_item" {
  http_method = aws_api_gateway_method.uploads_original_item.http_method
  resource_id = aws_api_gateway_resource.uploads_original_item.id
  rest_api_id = aws_api_gateway_rest_api.api.id
  status_code = "200"

  response_parameters = {
    "method.response.header.Cache-Control"             = true,
    "method.response.header.Content-Length"            = true,
    "method.response.header.Content-Type"              = true,
    "method.response.header.ETag"                      = true,
    "method.response.header.Last-Modified"             = true,
  }
}

resource "aws_api_gateway_integration_response" "uploads_original_item" {
  http_method      = aws_api_gateway_method.uploads_original_item.http_method
  resource_id      = aws_api_gateway_resource.uploads_original_item.id
  rest_api_id      = aws_api_gateway_rest_api.api.id
  status_code      = aws_api_gateway_method_response.uploads_original_item.status_code
  content_handling = "CONVERT_TO_BINARY"

  response_parameters = {
    "method.response.header.Cache-Control"             = "'max-age=31536000'"
    "method.response.header.Content-Length"            = "integration.response.header.Content-Length",
    "method.response.header.Content-Type"              = "integration.response.header.Content-Type",
    "method.response.header.ETag"                      = "integration.response.header.ETag",
    "method.response.header.Last-Modified"             = "integration.response.header.Last-Modified",
  }

  depends_on = [aws_api_gateway_integration.uploads_original_item]
}

resource "aws_api_gateway_method_response" "cached_uploads_original_item" {
  http_method = aws_api_gateway_method.uploads_original_item.http_method
  resource_id = aws_api_gateway_resource.uploads_original_item.id
  rest_api_id = aws_api_gateway_rest_api.api.id
  status_code = "304"

  response_parameters = {
    "method.response.header.Cache-Control"  = true,
    "method.response.header.ETag"           = true,
    "method.response.header.Last-Modified"  = true,
  }
}

resource "aws_api_gateway_integration_response" "cached_uploads_original_item" {
  http_method       = aws_api_gateway_method.uploads_original_item.http_method
  resource_id       = aws_api_gateway_resource.uploads_original_item.id
  rest_api_id       = aws_api_gateway_rest_api.api.id
  status_code       = aws_api_gateway_method_response.cached_uploads_original_item.status_code
  content_handling  = "CONVERT_TO_BINARY"
  selection_pattern = "304"

  response_parameters = {
    "method.response.header.Cache-Control"  = "'max-age=31536000'",
    "method.response.header.ETag"           = "integration.response.header.ETag",
    "method.response.header.Last-Modified"  = "integration.response.header.Last-Modified",
  }

  depends_on = [aws_api_gateway_integration.uploads_original_item]
}

# ############# #
# Static Assets #
# ############# #

data "aws_iam_policy_document" "assets_s3_policy" {
  statement {
    actions = [
      "s3:Get*",
      "s3:List*",
    ]

    resources = [
      aws_s3_bucket.static_assets.arn,
      "${aws_s3_bucket.static_assets.arn}/*"
    ]
  }
}

resource "aws_iam_policy" "assets_s3_policy" {
  name        = "${local.name_prefix}-asset-bucket-access"
  description = "Read only access for Plum Tree ${terraform.workspace} static assets bucket"
  policy      = data.aws_iam_policy_document.assets_s3_policy.json
}

data "aws_iam_policy_document" "assets_s3_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["apigateway.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "assets_s3_api_gateyway_role" {
  name = "${local.name_prefix}-gateway-assets-s3-role"
  assume_role_policy = data.aws_iam_policy_document.assets_s3_assume_role.json
}

resource "aws_iam_role_policy_attachment" "assets_s3_policy_attach" {
  role       = aws_iam_role.assets_s3_api_gateyway_role.name
  policy_arn = aws_iam_policy.assets_s3_policy.arn
}

resource "aws_api_gateway_resource" "assets" {
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "assets"
  rest_api_id = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_resource" "assets_item" {
  parent_id   = aws_api_gateway_resource.assets.id
  path_part   = "{item+}"
  rest_api_id = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_method" "assets_item" {
  authorization = "NONE"
  http_method   = "GET"
  resource_id   = aws_api_gateway_resource.assets_item.id
  rest_api_id   = aws_api_gateway_rest_api.api.id

  request_parameters = {
    "method.request.path.item" = true
  }
}

resource "aws_api_gateway_integration" "assets_item" {
  http_method             = aws_api_gateway_method.assets_item.http_method
  resource_id             = aws_api_gateway_resource.assets_item.id
  rest_api_id             = aws_api_gateway_rest_api.api.id
  type                    = "AWS"
  integration_http_method = "GET"
  uri                     = "arn:aws:apigateway:eu-west-1:s3:path/${aws_s3_bucket.static_assets.id}/{item}"
  credentials             = aws_iam_role.assets_s3_api_gateyway_role.arn

  request_parameters = {
    "integration.request.path.item" = "method.request.path.item"
  }
}

resource "aws_api_gateway_method_response" "assets_item" {
  http_method = aws_api_gateway_method.assets_item.http_method
  resource_id = aws_api_gateway_resource.assets_item.id
  rest_api_id = aws_api_gateway_rest_api.api.id
  status_code = "200"

  response_parameters = {
    "method.response.header.Content-Length" = true,
    "method.response.header.Content-Type" = true,
    "method.response.header.Strict-Transport-Security" = true,
    "method.response.header.X-Content-Type-Options" = true,
  }
}

resource "aws_api_gateway_integration_response" "assets_item" {
  http_method      = aws_api_gateway_method.assets_item.http_method
  resource_id      = aws_api_gateway_resource.assets_item.id
  rest_api_id      = aws_api_gateway_rest_api.api.id
  status_code      = aws_api_gateway_method_response.assets_item.status_code
  content_handling = "CONVERT_TO_BINARY"

  response_parameters = {
    "method.response.header.Content-Length" = "integration.response.header.Content-Length",
    "method.response.header.Content-Type" = "integration.response.header.Content-Type",
    "method.response.header.Strict-Transport-Security" = "'max-age=31536000; includeSubDomains; preload'",
    "method.response.header.X-Content-Type-Options" = "'nosniff'",
  }

  depends_on = [aws_api_gateway_integration.assets_item]
}

# ############# #
# Root Resource #
# ############# #

resource "aws_api_gateway_method" "root_item" {
  authorization = "NONE"
  http_method   = "GET"
  resource_id   = aws_api_gateway_rest_api.api.root_resource_id
  rest_api_id   = aws_api_gateway_rest_api.api.id

  request_parameters = {
    "method.request.path.item" = true
  }
}

resource "aws_api_gateway_integration" "root_item" {
  http_method             = aws_api_gateway_method.root_item.http_method
  resource_id             = aws_api_gateway_rest_api.api.root_resource_id
  rest_api_id             = aws_api_gateway_rest_api.api.id
  type                    = "AWS"
  integration_http_method = "GET"
  uri                     = "arn:aws:apigateway:eu-west-1:s3:path/${aws_s3_bucket.static_assets.id}/index.html"
  credentials             = aws_iam_role.assets_s3_api_gateyway_role.arn
}

resource "aws_api_gateway_method_response" "root_item" {
  http_method = aws_api_gateway_method.root_item.http_method
  resource_id = aws_api_gateway_rest_api.api.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.api.id
  status_code = "200"

  response_parameters = {
    "method.response.header.Content-Length" = true,
    "method.response.header.Content-Type" = true,
    "method.response.header.Strict-Transport-Security" = true,
    "method.response.header.X-Content-Type-Options" = true,
  }
}

resource "aws_api_gateway_integration_response" "root_item" {
  http_method      = aws_api_gateway_method.root_item.http_method
  resource_id      = aws_api_gateway_rest_api.api.root_resource_id
  rest_api_id      = aws_api_gateway_rest_api.api.id
  status_code      = aws_api_gateway_method_response.root_item.status_code
  content_handling = "CONVERT_TO_BINARY"

  response_parameters = {
    "method.response.header.Content-Length" = "integration.response.header.Content-Length",
    "method.response.header.Content-Type" = "integration.response.header.Content-Type",
    "method.response.header.Strict-Transport-Security" = "'max-age=31536000; includeSubDomains; preload'",
    "method.response.header.X-Content-Type-Options" = "'nosniff'",
  }

  depends_on = [aws_api_gateway_integration.root_item]
}

# ############ #
# Any Resource #
# ############ #

resource "aws_api_gateway_resource" "index" {
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "{any+}"
  rest_api_id = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_method" "index" {
  authorization = "NONE"
  http_method   = "GET"
  resource_id   = aws_api_gateway_resource.index.id
  rest_api_id   = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_integration" "index" {
  http_method             = aws_api_gateway_method.index.http_method
  resource_id             = aws_api_gateway_resource.index.id
  rest_api_id             = aws_api_gateway_rest_api.api.id
  type                    = "AWS"
  integration_http_method = "GET"
  uri                     = "arn:aws:apigateway:eu-west-1:s3:path/${aws_s3_bucket.static_assets.id}/index.html"
  credentials             = aws_iam_role.assets_s3_api_gateyway_role.arn
}

resource "aws_api_gateway_method_response" "index" {
  http_method = aws_api_gateway_method.index.http_method
  resource_id = aws_api_gateway_resource.index.id
  rest_api_id = aws_api_gateway_rest_api.api.id
  status_code = "200"

  response_parameters = {
    "method.response.header.Content-Length" = true,
    "method.response.header.Content-Type" = true,
    "method.response.header.Strict-Transport-Security" = true,
    "method.response.header.X-Content-Type-Options" = true,
  }
}

resource "aws_api_gateway_integration_response" "index" {
  http_method      = aws_api_gateway_method.index.http_method
  resource_id      = aws_api_gateway_resource.index.id
  rest_api_id      = aws_api_gateway_rest_api.api.id
  status_code      = aws_api_gateway_method_response.index.status_code
  content_handling = "CONVERT_TO_BINARY"

  response_parameters = {
    "method.response.header.Content-Length" = "integration.response.header.Content-Length",
    "method.response.header.Content-Type" = "integration.response.header.Content-Type",
    "method.response.header.Strict-Transport-Security" = "'max-age=31536000; includeSubDomains; preload'",
    "method.response.header.X-Content-Type-Options" = "'nosniff'",
  }

  depends_on = [aws_api_gateway_integration.index]
}

# ###### #
# Domain #
# ###### #

resource "aws_api_gateway_domain_name" "domain" {
  domain_name              = aws_acm_certificate.certificate.domain_name
  regional_certificate_arn = aws_acm_certificate_validation.certificate.certificate_arn
  security_policy          = "TLS_1_2"

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_deployment" "live" {
  depends_on = [
    aws_api_gateway_integration_response.healthcheck,
    // todo possibly add other aws_api_gateway_integration_response resources
  ]
  rest_api_id = aws_api_gateway_rest_api.api.id
  stage_name  = "live"
  description = "Temporary deployment to connect the base path mapping for custom domains"
}

resource "aws_api_gateway_base_path_mapping" "live" {
  api_id      = aws_api_gateway_rest_api.api.id
  stage_name  = aws_api_gateway_deployment.live.stage_name
  domain_name = aws_api_gateway_domain_name.domain.domain_name
}
