# all user uploads land in the processing bucket. If a item is saved the images
# is moved to the processed bucket. Otherwise items in the input bucket with the
# temp tag are deleted after 7 days (note some legacy items don't have this
# tag).
resource "aws_s3_bucket" "input_bucket" {
  bucket = "com.theplumtreeapp.upload-input"
  tags   = var.tags
}

resource "aws_s3_bucket_lifecycle_configuration" "temp" {
  bucket = aws_s3_bucket.input_bucket.id

  rule {
    id     = "temp"
    status = "Enabled"

    expiration {
      days = 2
    }
  }
}

# Add cors rules so we can upload via pre-signed URLs
resource "aws_s3_bucket_cors_configuration" "upload" {
  bucket = aws_s3_bucket.input_bucket.id

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "PUT"]
    allowed_origins = ["*"]
    max_age_seconds = 3000
  }
}

resource "aws_s3_bucket_public_access_block" "input_bucket" {
  block_public_acls       = false
  block_public_policy     = false
  bucket                  = aws_s3_bucket.input_bucket.id
  ignore_public_acls      = false
  restrict_public_buckets = false
}
