variable "tags" {
  type        = map(string)
  description = "Tags to apply to all resources"
}

variable "domain" {
  type = string
  description = "The application web domain"
  default = "theplumtreeapp.com"
}

variable "color" {
  type = string
  description = "The color env to make live"
}
